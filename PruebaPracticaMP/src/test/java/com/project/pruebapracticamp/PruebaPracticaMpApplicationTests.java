package com.project.pruebapracticamp;

import com.project.pruebapracticamp.application.service.CompanyService;
import com.project.pruebapracticamp.application.service.CountryService;
import com.project.pruebapracticamp.controller.Controller;
import com.project.pruebapracticamp.infraestructure.dbo.Company;
import com.project.pruebapracticamp.infraestructure.dbo.Country;
import com.project.pruebapracticamp.infraestructure.request.CompanyRequest;
import com.project.pruebapracticamp.infraestructure.response.ResponseMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.List;

@SpringBootTest
class PruebaPracticaMpApplicationTests {

    @Autowired private Controller mainController;
    @Autowired private CompanyService companyService;
    @Autowired private CountryService countryService;

    @Test
    void contextLoads() {}

    @Test   // Prueba si el servicio de busqueda de compañias no devuelve ninguna excepcion
    void getCompaniesWithNoExceptionsTst() {
        Assertions.assertDoesNotThrow(() -> {
            mainController.getCompanies(null, null);
        });
    }

    @Test   // Prueba si el servicio de busqueda de paises no devuelve ninguna excepcion
    void getCountriesWithNoExceptionsTst() {
        Assertions.assertDoesNotThrow(() -> {
            mainController.getCountries(null);
        });
    }

    @Test   // Prueba si el servicio de listar pais por id no devuelve ninguna excepcion
    void findCountryByIdTst() {
        Country country = countryService.findByCountryId(1L);

        Assertions.assertNotNull(country);
        Assertions.assertFalse(country.getCountryCode() < 0);
        Assertions.assertNotNull(country.getCountryName());
    }

    @Test   // Prueba si el servicio de listar compañias por id no devuelve ninguna excepcion
    void findCompanyByIdTst() {
        Company com = companyService.findByCompanyId(5L);

        Assertions.assertNotNull(com);
        Assertions.assertNotNull(com.getCountryId());
        Assertions.assertNotNull(com.getCompanyName());
        Assertions.assertNotNull(com.getCompanyTaxcode());
    }

    @Test   // Prueba si el servicio de listar compañias por nombre no devuelve ninguna excepcion
    void findCompanyByNameTst() {
        List<Company> com = companyService.searchByName(null);

        Assertions.assertNotNull(com);
    }

    @Test   // Prueba si la elminacion se ejecuta sin excepciones
    void deleteCompaniesWithNoExceptionsTst() {
        Assertions.assertDoesNotThrow(() -> {
            mainController.deleteCompany(16L);
        });
    }

    @Test   // Verifica si el guardado no devuelve ninguna excepcion
    void saveCompanyWithNoExceptionsTst() {
        CompanyRequest req = new CompanyRequest();

        req.setCompanyId(null);
        req.setCompanyName("TEST");
        req.setTaxCode("00-000-000-0");
        req.setCountryId(1L);

        Assertions.assertDoesNotThrow(() -> {
            mainController.saveCompany(req);
        });
    }

    @Test   // Prueba el guardado de una nueva compañia
    void saveCompanyTst() {
        CompanyRequest req = new CompanyRequest();

        req.setCompanyId(null);
        req.setCompanyName("TEST");
        req.setTaxCode("00-000-000-0");
        req.setCountryId(1L);

        ResponseEntity<ResponseMapper> response =
                (ResponseEntity<ResponseMapper>) mainController.saveCompany(req);

        Assertions.assertNotNull(response);
        Assertions.assertNotNull(response.getBody());
        Assertions.assertFalse(response.getBody().getCode() < 0);
    }

    @Test   // Prueba si da devuelve error 204 al no encontrar la compañia
    void editCompanyNotExistentTst() {
        CompanyRequest req = new CompanyRequest();

        req.setCompanyId(1000L);
        req.setCompanyName("TEST");
        req.setTaxCode("00-000-000-0");
        req.setCountryId(1L);

        ResponseEntity<ResponseMapper> response =
                (ResponseEntity<ResponseMapper>) mainController.saveCompany(req);

        Assertions.assertNotNull(response);
        Assertions.assertNotNull(response.getBody());
        Assertions.assertEquals(204, response.getBody().getCode());
    }

}
