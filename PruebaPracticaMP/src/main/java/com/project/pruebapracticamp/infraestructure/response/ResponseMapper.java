package com.project.pruebapracticamp.infraestructure.response;

public class ResponseMapper {

    Integer code;
    String msg;
    Object result;

    public ResponseMapper() {
    }

    public ResponseMapper(Integer code, String msg, Object obj) {
        this.code = code;
        this.msg = msg;
        this.result = obj;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
