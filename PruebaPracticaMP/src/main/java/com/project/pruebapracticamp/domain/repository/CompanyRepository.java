package com.project.pruebapracticamp.domain.repository;

import com.project.pruebapracticamp.infraestructure.dbo.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    // Named queries
	List<Company> findAllCompanies();
    Optional<Company> findByCompanyId(Long companyId);


    // Custom queries
    @Query("select C from Company C where UPPER(C.companyName) LIKE %?1%")
    List<Company> findByCompanyName(String name);

}
