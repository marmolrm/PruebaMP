package com.project.pruebapracticamp.domain.repository;

import com.project.pruebapracticamp.infraestructure.dbo.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

    // Named queries
	List<Country> findAllCountries();
    Optional<Country> findByCountryId(Long countryId);

}
