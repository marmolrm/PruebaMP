package com.project.pruebapracticamp.application.service.impl;

import com.project.pruebapracticamp.domain.repository.CountryRepository;
import com.project.pruebapracticamp.application.service.CountryService;
import com.project.pruebapracticamp.infraestructure.dbo.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    Logger log = LoggerFactory.getLogger(CountryServiceImpl.class);

    @Autowired private CountryRepository repository;

    @Override
    public List<Country> listAll() {
        try {
            return repository.findAllCountries();
        } catch (Exception e) {
            log.error("listAll error: {}", e.getLocalizedMessage());
            return Collections.emptyList();
        }
    }

    @Override
    public Country findByCountryId(Long id) {
        try {
            if (id != null)
                return repository.findByCountryId(id).orElse(null);
            else return null;
        } catch (Exception e) {
            log.error("findByCountryId error: {}", e.getLocalizedMessage());
            return null;
        }
    }
}
