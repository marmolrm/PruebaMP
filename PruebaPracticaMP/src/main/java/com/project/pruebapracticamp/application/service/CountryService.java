package com.project.pruebapracticamp.application.service;

import com.project.pruebapracticamp.infraestructure.dbo.Country;

import java.util.List;

public interface CountryService {

    List<Country> listAll();
    Country findByCountryId(Long id);

}
