package com.project.pruebapracticamp.application.service.impl;

import com.project.pruebapracticamp.domain.repository.CompanyRepository;
import com.project.pruebapracticamp.application.service.CompanyService;
import com.project.pruebapracticamp.infraestructure.dbo.Company;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {

    Logger log = LoggerFactory.getLogger(CompanyServiceImpl.class);

    @Autowired private CompanyRepository repository;

    @Override
    public Company save(Company com) {
        try {
            return repository.save(com);
        } catch (Exception e) {
            log.error("save error: {}", e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public Company findByCompanyId(Long id) {
        try {
            return repository.findByCompanyId(id).orElse(null);
        } catch (Exception e) {
            log.error("findByCompanyId error: {}", e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public List<Company> listAll() {
        try {
            return repository.findAllCompanies();
        } catch (Exception e) {
            log.error("listAll error: {}", e.getLocalizedMessage());
            return Collections.emptyList();
        }
    }

    @Override
    public List<Company> searchByName(String name) {
        try {
            if (name  != null)
                return repository.findByCompanyName(name.toUpperCase());
            else return Collections.emptyList();
        } catch (Exception e) {
            log.error("searchByName error: {}", e.getLocalizedMessage());
            return Collections.emptyList();
        }
    }

    @Override
    public boolean delete(Company com) {
        try {
            if (com != null)
                repository.deleteById(com.getCompanyId());
            else return false;
            return !(repository.existsById(com.getCompanyId()));
        } catch (Exception e) {
            log.error("delete error: {}", e.getLocalizedMessage());
            return false;
        }
    }
}
