package com.project.pruebapracticamp.application.service;

import com.project.pruebapracticamp.infraestructure.dbo.Company;

import java.util.List;

public interface CompanyService {

    Company save(Company com);
    Company findByCompanyId(Long id);
    List<Company> listAll();
    List<Company> searchByName(String name);
    boolean delete(Company com);

}
