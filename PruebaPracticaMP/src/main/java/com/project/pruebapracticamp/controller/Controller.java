package com.project.pruebapracticamp.controller;

import com.project.pruebapracticamp.application.service.CompanyService;
import com.project.pruebapracticamp.application.service.CountryService;
import com.project.pruebapracticamp.infraestructure.dbo.Company;
import com.project.pruebapracticamp.infraestructure.dbo.Country;
import com.project.pruebapracticamp.infraestructure.request.CompanyRequest;
import com.project.pruebapracticamp.infraestructure.response.ResponseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("prueba")
public class Controller {

    @Autowired private CountryService countryService;
    @Autowired private CompanyService companyService;

    @GetMapping("getCountries")
    public ResponseEntity<?> getCountries(@RequestParam(required = false) Long id) {

        if (id == null)
            return new ResponseEntity<>(
                    new ResponseMapper(0, "Ok", countryService.listAll()), HttpStatus.OK);
        else {
            List<Country> list = new ArrayList<>();
            Country country = countryService.findByCountryId(id);
            if (country != null)
                list.add(country);

            return new ResponseEntity<>(
                    new ResponseMapper(0, "Ok", list), HttpStatus.OK);
        }
    }

    @GetMapping("getCompanies")
    public ResponseEntity<?> getCompanies(
            @RequestParam(required = false) Long id,
            @RequestParam(required = false) String name)
    {
        if (id != null && name != null)
            return new ResponseEntity<>(
                    new ResponseMapper(10, "No puede combinar parámetros", Collections.emptyList()),
                    HttpStatus.BAD_REQUEST);

        if (id != null) {
            List<Company> list = new ArrayList<>();
            Company company = companyService.findByCompanyId(id);
            if (company != null)
                list.add(company);

            return new ResponseEntity<>(
                    new ResponseMapper(0, "Ok", list), HttpStatus.OK);
        } else if (name != null) {
            return new ResponseEntity<>(
                    new ResponseMapper(0, "Ok", companyService.searchByName(name)), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(
                    new ResponseMapper(0, "Ok", companyService.listAll()), HttpStatus.OK);
        }
    }

    @PostMapping("saveCompany")
    public ResponseEntity<?> saveCompany(@RequestBody CompanyRequest request) {

        Company company;
        Country countryID = countryService.findByCountryId(request.getCountryId());

        if (countryID == null)
            return new ResponseEntity<>(
                    new ResponseMapper(10, "País no es válido", request), HttpStatus.OK);

        if (request.getCompanyId() == null) // Compania nueva
            company = new Company();
        else                                // Edicion
            company = companyService.findByCompanyId(request.getCompanyId());

        if (company == null)
            return new ResponseEntity<>(
                    new ResponseMapper(204, "No se encontró la compañía", request), HttpStatus.OK);

        company.setCountryId(countryID);

        if (request.getCompanyName() != null)
            company.setCompanyName(request.getCompanyName());
        if (request.getTaxCode() != null)
            company.setCompanyTaxcode(request.getTaxCode());

        company = companyService.save(company);     // Guardado

        if (company != null)
            return new ResponseEntity<>(
                    new ResponseMapper(0, "Ok", company), HttpStatus.OK);
        else
            return new ResponseEntity<>(
                    new ResponseMapper(90, "Ocurrió un error", request), HttpStatus.OK);
    }

    @DeleteMapping("deleteCompany")
    public ResponseEntity<?> deleteCompany(@RequestParam Long id) {
        Company company = companyService.findByCompanyId(id);
        if (company == null)
            return new ResponseEntity<>(
                    new ResponseMapper(400, "No se encontró la compañía", company), HttpStatus.BAD_REQUEST);

        if (companyService.delete(company))
            return new ResponseEntity<>(new ResponseMapper(0, "Ok", company), HttpStatus.OK);
        else
            return new ResponseEntity<>(new ResponseMapper(90, "Ocurrió un error", company), HttpStatus.OK);
    }

}
