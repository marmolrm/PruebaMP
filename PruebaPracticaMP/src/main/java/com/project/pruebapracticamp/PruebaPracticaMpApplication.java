package com.project.pruebapracticamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaPracticaMpApplication {

    public static void main(String[] args) {
        SpringApplication.run(PruebaPracticaMpApplication.class, args);
    }

}
